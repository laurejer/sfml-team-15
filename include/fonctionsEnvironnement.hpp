#ifndef FONCTIONSENVIRONNEMENT_HPP_INCLUDED
#define FONCTIONSENVIRONNEMENT_HPP_INCLUDED
#include "Stone.hpp"
#include "Hole.h"
#include "CONSTANTES.hpp"

Entite* creerEnvironnement(Vector2f position, Vector2f taille, Texture &texture, TypeEntite type);

#endif // FONCTIONSENVIRONNEMENT_HPP_INCLUDED

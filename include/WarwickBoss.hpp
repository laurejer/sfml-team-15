#ifndef WARWICKBOSS_HPP_INCLUDED
#define WARWICKBOSS_HPP_INCLUDED

#include "Ennemi.hpp"

class WarwickBoss : public Ennemi
{
    public:
        WarwickBoss();
        WarwickBoss(Vector2f position, Vector2f taille, int vieDepart, bool estMort) : Ennemi(position, taille, vieDepart, estMort)
        {}
        virtual ~WarwickBoss();

    protected:

    private:
};

#endif // WARWICKBOSS_HPP_INCLUDED
